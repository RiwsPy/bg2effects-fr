# BG2Effects-FR

Proposition de traduction française du fichier BG2Effects.dat du logiciel DLTCEP.
Optimisé pour BG2EE v2.6.6.0

Les opcodes sont complétés en fonction des connaissances actuelles de l'auteur, épaulé par :
https://gibberlings3.github.io/iesdp/opcodes/bgee.htm

Complété par des dizaines d'heures de tests, de tentatives et autres joyeusetés de cet acabit.
Vérifié, modifié, complété par des dizaines d'heures de reverse engineering sur le moteur de jeu.

Le résultat est donc le croisement de plusieurs méthodologies, sources réputées fiables et de travail personnel.
